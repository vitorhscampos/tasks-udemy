package com.devmasterteam.tasks.entities;

import java.util.AbstractMap;
import java.util.HashMap;

public class FullParameters {

    public String method;
    public String url;
    public AbstractMap<String, String> headerParameters;
    public AbstractMap<String, String> parameters;

    public FullParameters(String method, String url, HashMap params, HashMap headerParams){
        this.method = method;
        this.url = url;
        this.headerParameters = headerParams;
        this.parameters = params;
    }


}
