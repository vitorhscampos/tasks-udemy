package com.devmasterteam.tasks.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.devmasterteam.tasks.R;
import com.devmasterteam.tasks.adapter.TaskListAdapter;
import com.devmasterteam.tasks.constants.TaskConstants;
import com.devmasterteam.tasks.entities.TaskEntity;
import com.devmasterteam.tasks.infra.operations.OperationListener;
import com.devmasterteam.tasks.listeners.TaskListInteractionListener;
import com.devmasterteam.tasks.manager.TaskManager;

import java.util.ArrayList;
import java.util.List;

public class TaskListFragment extends Fragment implements View.OnClickListener {

    private int  mFilter;
    private Context mContext;
    private TaskManager mTaskManager;
    private List<TaskEntity> mTaskEntityList;
    private TaskListAdapter mTaskListAdapter;
    private ViewHolder mViewHolder = new ViewHolder();
    private TaskListInteractionListener mTaskListInteractionListener;

    public static TaskListFragment newInstance(int filter) {
        TaskListFragment fragment = new TaskListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(TaskConstants.TASK_FILTER.KEY, filter);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null){
            this.mFilter = getArguments().getInt(TaskConstants.TASK_FILTER.KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Infla o layout
        View rootView = inflater.inflate(R.layout.fragment_task_list, container, false);

        // Incializa as variáveis
        this.mContext = rootView.getContext();
        this.mTaskManager = new TaskManager(this.mContext);

        // Inicializa elementos
        this.mViewHolder.floatAddTask = rootView.findViewById(R.id.float_add_task);

        // Inicializa eventos
        this.mViewHolder.floatAddTask.setOnClickListener(this);

        this.mTaskListInteractionListener = new TaskListInteractionListener() {
            @Override
            public void onListClick(int id) {
                Bundle bundle = new Bundle();
                bundle.putInt(TaskConstants.BUNDLE.TASK_ID, id);

                Intent intent = new Intent(mContext, TaskFormActivity.class);
                intent.putExtras(bundle);

                startActivity(intent);
            }

            @Override
            public void onCompleteClick(int id) {
                mTaskManager.complete(id, true, taskUpdatedListener() );
            }

            @Override
            public void onIncompleteClick(int id) {
                mTaskManager.complete(id, false, taskUpdatedListener() );
            }

            @Override
            public void onDeleteCLick(int id) {
                mTaskManager.delete(id, taskDeletedListener());
            }
        };

        // 1 - Obter a recyclerview
        this.mViewHolder.recylerTaskList = rootView.findViewById(R.id.recycler_task_list);

        // 2 - Definir adapter passando listagem de itens
        this.mTaskEntityList = new ArrayList<>();
        this.mTaskListAdapter = new TaskListAdapter(this.mTaskEntityList, mTaskListInteractionListener);
        this.mViewHolder.recylerTaskList.setAdapter(mTaskListAdapter);

        // 3 - Definir um layout
        this.mViewHolder.recylerTaskList.setLayoutManager(new LinearLayoutManager(this.mContext));

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        this.getTasks();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.float_add_task) {
            startActivity(new Intent(this.mContext, TaskFormActivity.class));
        }
    }

    private void getTasks(){
        this.mTaskEntityList = new ArrayList<>();
        this.mTaskManager.getList(mFilter, taskLoadedListener());
    }

    private OperationListener taskLoadedListener(){
        return new OperationListener<List<TaskEntity>>(){
            @Override
            public void onSuccess(List<TaskEntity> result){
                mTaskEntityList.addAll(result);
                mTaskListAdapter = new TaskListAdapter(mTaskEntityList, mTaskListInteractionListener);
                mViewHolder.recylerTaskList.setAdapter(mTaskListAdapter);
                mTaskListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(int errorCode, String errorMessage){
                Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();
            }
        };
    }

    private OperationListener taskUpdatedListener(){
        return new OperationListener<Boolean>(){
            @Override
            public void onSuccess(Boolean result){
                getTasks();
            }

            @Override
            public void onError(int errorCode, String errorMessage){
                Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();
            }
        };
    }

    private OperationListener taskDeletedListener(){
        return new OperationListener<Boolean>(){
            @Override
            public void onSuccess(Boolean result){
                Toast.makeText(mContext, R.string.tarefa_removida_com_sucesso, Toast.LENGTH_LONG).show();
                getTasks();
            }

            @Override
            public void onError(int errorCode, String errorMessage){
                Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();
            }
        };
    }

    private static class ViewHolder {
        private FloatingActionButton floatAddTask;
        private RecyclerView recylerTaskList;
    }
}
