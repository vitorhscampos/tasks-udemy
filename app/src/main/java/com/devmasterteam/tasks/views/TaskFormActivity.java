package com.devmasterteam.tasks.views;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.devmasterteam.tasks.R;
import com.devmasterteam.tasks.constants.TaskConstants;
import com.devmasterteam.tasks.entities.PriorityEntity;
import com.devmasterteam.tasks.entities.TaskEntity;
import com.devmasterteam.tasks.infra.operations.OperationListener;
import com.devmasterteam.tasks.manager.PriorityManager;
import com.devmasterteam.tasks.manager.TaskManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class TaskFormActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private Context mContext;
    private int mTaskId = 0;
    private TaskManager mTaskManager;
    List<PriorityEntity> mPriorityEntityList;
    private PriorityManager mPriorityManager;
    private ArrayList<Integer> mListPriorityId;
    private ViewHolder mViewHolder = new ViewHolder();
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_form);

        this.mPriorityManager = new PriorityManager(this);
        this.mTaskManager = new TaskManager(this);
        this.mContext = this;
        this.mListPriorityId = new ArrayList<>();

        // Inicializa variáveis
        this.mViewHolder.editDescription = this.findViewById(R.id.edit_description);
        this.mViewHolder.checkComplete = this.findViewById(R.id.check_complete);
        this.mViewHolder.spinnerPriority = this.findViewById(R.id.spinner_priority);
        this.mViewHolder.buttonDate = this.findViewById(R.id.button_date);
        this.mViewHolder.buttonSave = this.findViewById(R.id.button_save);
        this.mViewHolder.progressDialog = new ProgressDialog(this);

        // Atribui eventos
        this.mViewHolder.buttonSave.setOnClickListener(this);
        this.mViewHolder.buttonDate.setOnClickListener(this);

        this.loadPriorities();
        this.loadDataFromActivity();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.button_save) {
            this.handleSave();
        } else if (id == R.id.button_date) {
            this.showDatePicker();
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        this.mViewHolder.buttonDate.setText(SIMPLE_DATE_FORMAT.format(calendar.getTime()));
    }

    private void loadDataFromActivity() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            this.mTaskId = bundle.getInt(TaskConstants.BUNDLE.TASK_ID);
            this.mTaskManager.get(mTaskId, taskLoadedListener());
        }
    }

    private OperationListener taskLoadedListener(){
        return new OperationListener<TaskEntity>(){
            @Override
            public void onSuccess(TaskEntity result){
                mViewHolder.buttonDate.setText(SIMPLE_DATE_FORMAT.format(result.DueDate));
                mViewHolder.checkComplete.setChecked(result.Complete);
                mViewHolder.editDescription.setText(result.Description);
                mViewHolder.spinnerPriority.setSelection(getIndex(result.PriorityId));
            }

            @Override
            public void onError(int errorCode, String errorMessage){
                showLoading(false, "", "");
                Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();
            }
        };
    }

    private void showDatePicker() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(this, this, year, month, dayOfMonth).show();
    }

    private void loadPriorities() {
        this.mPriorityEntityList = this.mPriorityManager.getListLocal();

        List<String> lstDescription = new ArrayList<>();

        for(PriorityEntity entity : this.mPriorityEntityList){
            lstDescription.add(entity.Description);
            mListPriorityId.add(entity.Id);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, lstDescription);

        this.mViewHolder.spinnerPriority.setAdapter(adapter);
    }

    private void handleSave(){
        this.showLoading(true, getString(R.string.salvando), getString(R.string.salvando_tarefa));
        try{
            TaskEntity entity = new TaskEntity();

            entity.Id = this.mTaskId;
            entity.Description = this.mViewHolder.editDescription.getText().toString();
            entity.Complete = this.mViewHolder.checkComplete.isChecked();
            entity.PriorityId = this.mListPriorityId.get(this.mViewHolder.spinnerPriority.getSelectedItemPosition());

            if (!"".equals(this.mViewHolder.buttonDate.getText().toString())){
                entity.DueDate = SIMPLE_DATE_FORMAT.parse(this.mViewHolder.buttonDate.getText().toString());
            }

            if (this.mTaskId == 0){
                this.mTaskManager.insert(entity, taskSavedListener());
            }else {
                this.mTaskManager.update(entity, taskSavedListener());
            }

        }catch (Exception e){
            Toast.makeText(this, R.string.UNEXPECTED_ERROR, Toast.LENGTH_SHORT).show();
            this.showLoading(false, "", "");
        }
    }

    private OperationListener taskSavedListener(){
        return new OperationListener<Boolean>(){
            @Override
            public void onSuccess(Boolean result){
                showLoading(false, "", "");
                if (mTaskId == 0){
                    Toast.makeText(mContext, R.string.tarefa_incluida_com_sucesso, Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(mContext, R.string.tarefa_atualizada_com_sucesso, Toast.LENGTH_LONG).show();
                }
                finish();
            }

            @Override
            public void onError(int errorCode, String errorMessage){
                showLoading(false, "", "");
                Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();
            }
        };
    }

    private int getIndex (int priorityId){
        int index = 0;
        for(int i = 0; i < this.mPriorityEntityList.size(); i++){
            if (this.mPriorityEntityList.get(i).Id == priorityId){
                index = i;
                break;
            }
        }

        return index;
    }

    private void showLoading (Boolean show, String title, String message){
        if (show){
            this.mViewHolder.progressDialog.setTitle(title);
            this.mViewHolder.progressDialog.setMessage(message);
            this.mViewHolder.progressDialog.show();
        }else{
            this.mViewHolder.progressDialog.hide();
            this.mViewHolder.progressDialog.dismiss();
        }
    }

    private static class ViewHolder {
        private EditText editDescription;
        private CheckBox checkComplete;
        private Spinner spinnerPriority;
        private Button buttonDate;
        private Button buttonSave;
        private ProgressDialog progressDialog;
    }
}