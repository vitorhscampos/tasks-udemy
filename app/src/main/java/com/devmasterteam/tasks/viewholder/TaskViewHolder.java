package com.devmasterteam.tasks.viewholder;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.devmasterteam.tasks.R;
import com.devmasterteam.tasks.constants.PriorityCacheConstants;
import com.devmasterteam.tasks.entities.TaskEntity;
import com.devmasterteam.tasks.listeners.TaskListInteractionListener;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class TaskViewHolder extends RecyclerView.ViewHolder {

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

    private TextView mTextDescription;
    private TextView mTextPriorityId;
    private TextView mTextPriority;
    private TextView mTextDueDate;
    private ImageView mImageTask;
    private Context mContext;

    public TaskViewHolder(View itemView) {
        super(itemView);

        this.mTextDescription = itemView.findViewById(R.id.text_description);
        this.mTextPriority = itemView.findViewById(R.id.text_priority);
        this.mTextPriorityId = itemView.findViewById(R.id.text_priority_id);
        this.mTextDueDate = itemView.findViewById(R.id.text_due_date);
        this.mImageTask = itemView.findViewById(R.id.image_task);
        this.mContext = itemView.getContext();
    }

    public void bindData(final TaskEntity entity, final TaskListInteractionListener listener){
        this.mTextDescription.setText(entity.Description);
        this.mTextPriorityId.setText(String.valueOf(entity.PriorityId));
        this.mTextPriority.setText(PriorityCacheConstants.get(entity.PriorityId));
        this.mTextDueDate.setText(SIMPLE_DATE_FORMAT.format(entity.DueDate));

        if (entity.Complete){
            this.mImageTask.setImageResource(R.drawable.ic_done);
        }

        this.mTextDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onListClick(entity.Id);
            }
        });

        this.mImageTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (entity.Complete) {
                    listener.onIncompleteClick(entity.Id);
                } else {
                    listener.onCompleteClick(entity.Id);
                }
            }
        });

        this.mTextDescription.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showConfirmationDialog(listener, entity);
                return true;
            }
        });
    }

    private void showConfirmationDialog(final TaskListInteractionListener listener, final TaskEntity entity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle(R.string.remocao_de_tarefa)
                .setMessage(mContext.getString(R.string.confirmacao_remocao, entity.Description))
                .setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onDeleteCLick(entity.Id);
                    }
                })
                .setNegativeButton(R.string.cancelar, null)
                .show();
    }

}
