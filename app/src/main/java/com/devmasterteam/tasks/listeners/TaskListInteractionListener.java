package com.devmasterteam.tasks.listeners;

public interface TaskListInteractionListener {

    void onListClick(int id);

    void onCompleteClick(int id);

    void onIncompleteClick(int id);

    void onDeleteCLick(int id);
}
