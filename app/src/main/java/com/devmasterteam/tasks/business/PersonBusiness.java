package com.devmasterteam.tasks.business;

import android.content.Context;

import com.devmasterteam.tasks.constants.TaskConstants;
import com.devmasterteam.tasks.entities.APIResponse;
import com.devmasterteam.tasks.entities.FullParameters;
import com.devmasterteam.tasks.entities.HeaderEntity;
import com.devmasterteam.tasks.infra.SecurityPreferences;
import com.devmasterteam.tasks.infra.URLBuilder;
import com.devmasterteam.tasks.infra.operations.OperationResult;
import com.devmasterteam.tasks.repository.api.ExternalRepository;
import com.devmasterteam.tasks.utils.FormatUrlParameter;
import com.google.gson.Gson;

import java.util.AbstractMap;
import java.util.HashMap;

public class PersonBusiness extends BaseBusiness{

    private ExternalRepository mExternalRepository;
    private Context mContext;

    public PersonBusiness(Context context) {
        super(context);
        this.mExternalRepository = new ExternalRepository(context);
        this.mContext = context;
    }

    public OperationResult<Boolean> create(String name, String email, String password){
        OperationResult<Boolean> result = new OperationResult<>();

        try{
            URLBuilder builder = new URLBuilder();
            builder.addResource(TaskConstants.ENDPOINT.AUTHENTICATION_CREATE);

            AbstractMap<String, String> params = new HashMap<>();

            params.put(TaskConstants.API_PARAMETER.NAME, name);
            params.put(TaskConstants.API_PARAMETER.EMAIL, email);
            params.put(TaskConstants.API_PARAMETER.PASSWORD, password);
            params.put(TaskConstants.API_PARAMETER.RECEIVE_NEWS, FormatUrlParameter.formatBoolean(false));

            FullParameters full = new FullParameters(TaskConstants.OPERATION_METHOD.POST, builder.getUrl(),(HashMap)params, null);

            APIResponse response = this.mExternalRepository.execute(full);

            if (response.statusCode == TaskConstants.STATUS_CODE.SUCCESS){
                HeaderEntity entity = new Gson().fromJson(response.json, HeaderEntity.class);

                SecurityPreferences preferences = new SecurityPreferences(mContext);

                preferences.storeString(TaskConstants.HEADER.PERSON_KEY, entity.personKey);
                preferences.storeString(TaskConstants.HEADER.TOKEN_KEY, entity.token);

                preferences.storeString(TaskConstants.USER.NAME, entity.name);
                preferences.storeString(TaskConstants.USER.EMAIL, email);

                result.setResult(true);
            }else{
                result.setError(super.handleStatusCode(response.statusCode), super.handleErrorMessage(response.json));
            }
        }catch (Exception e) {
            result.setError(super.handleExceptionCode(e), super.handleExceptionMessage(e));
        }

        return result;
    }

    public OperationResult<Boolean> login(String email, String password) {
        OperationResult<Boolean> result = new OperationResult<>();

        try{
            URLBuilder builder = new URLBuilder();
            builder.addResource(TaskConstants.ENDPOINT.AUTHENTICATION_LOGIN);

            AbstractMap<String, String> params = new HashMap<>();

            params.put(TaskConstants.API_PARAMETER.EMAIL, email);
            params.put(TaskConstants.API_PARAMETER.PASSWORD, password);

            FullParameters full = new FullParameters(TaskConstants.OPERATION_METHOD.POST, builder.getUrl(),(HashMap)params, null);

            APIResponse response = this.mExternalRepository.execute(full);

            if (response.statusCode == TaskConstants.STATUS_CODE.SUCCESS){
                HeaderEntity entity = new Gson().fromJson(response.json, HeaderEntity.class);

                SecurityPreferences preferences = new SecurityPreferences(mContext);

                preferences.storeString(TaskConstants.HEADER.PERSON_KEY, entity.personKey);
                preferences.storeString(TaskConstants.HEADER.TOKEN_KEY, entity.token);

                preferences.storeString(TaskConstants.USER.NAME, entity.name);
                preferences.storeString(TaskConstants.USER.EMAIL, email);

                result.setResult(true);
            }else{
                result.setError(super.handleStatusCode(response.statusCode), super.handleErrorMessage(response.json));
            }
        }catch (Exception e) {
            result.setError(super.handleExceptionCode(e), super.handleExceptionMessage(e));
        }

        return result;
    }
}
