package com.devmasterteam.tasks.business;

import android.content.Context;

import com.devmasterteam.tasks.R;
import com.devmasterteam.tasks.constants.TaskConstants;
import com.devmasterteam.tasks.infra.InternetNotAvailableException;
import com.devmasterteam.tasks.infra.SecurityPreferences;
import com.google.gson.Gson;

import java.util.AbstractMap;
import java.util.HashMap;

class BaseBusiness {

    private Context mContext;

    BaseBusiness(Context context) {
        this.mContext = context;
    }

    int handleExceptionCode(Exception e) {
        if (e instanceof InternetNotAvailableException){
            return TaskConstants.STATUS_CODE.INTERNET_NOT_AVAILABLE;
        }

        return TaskConstants.STATUS_CODE.INTERNAL_SERVER_ERROR;

    }

    String handleExceptionMessage(Exception e) {
        if (e instanceof InternetNotAvailableException){
            return mContext.getString(R.string.INTERNET_NOT_AVAILABLE);
        }
        return mContext.getString(R.string.UNEXPECTED_ERROR);
    }

    String handleErrorMessage(String json) {
        return new Gson().fromJson(json, String.class);

    }

    int handleStatusCode(int statusCode) {
        if (statusCode == TaskConstants.STATUS_CODE.FORBIDDEN){
            return TaskConstants.STATUS_CODE.FORBIDDEN;
        }else if (statusCode == TaskConstants.STATUS_CODE.NOT_FOUND){
            return TaskConstants.STATUS_CODE.NOT_FOUND;
        }else {
            return TaskConstants.STATUS_CODE.INTERNAL_SERVER_ERROR;
        }
    }

    AbstractMap<String, String> getHeaderParams() {

        SecurityPreferences preferences = new SecurityPreferences(mContext);

        AbstractMap<String, String> headerParams = new HashMap<>();

        headerParams.put(TaskConstants.HEADER.PERSON_KEY, preferences.getStoredString(TaskConstants.HEADER.PERSON_KEY));
        headerParams.put(TaskConstants.HEADER.TOKEN_KEY, preferences.getStoredString(TaskConstants.HEADER.TOKEN_KEY));

        return headerParams;

    }
}
