package com.devmasterteam.tasks.business;

import android.content.Context;

import com.devmasterteam.tasks.constants.TaskConstants;
import com.devmasterteam.tasks.entities.APIResponse;
import com.devmasterteam.tasks.entities.FullParameters;
import com.devmasterteam.tasks.entities.TaskEntity;
import com.devmasterteam.tasks.infra.URLBuilder;
import com.devmasterteam.tasks.infra.operations.OperationResult;
import com.devmasterteam.tasks.repository.api.ExternalRepository;
import com.devmasterteam.tasks.utils.FormatUrlParameter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;

public class TaskBusiness extends BaseBusiness {

    private final ExternalRepository mExternalRepository;

    public TaskBusiness(Context context) {
        super(context);
        this.mExternalRepository = new ExternalRepository(context);
    }

    public OperationResult<Boolean> insert(TaskEntity entity) {
        OperationResult<Boolean> result = new OperationResult<>();

        try{
            URLBuilder builder = new URLBuilder();
            builder.addResource(TaskConstants.ENDPOINT.TASK_INSERT);

            AbstractMap<String, String > headerParams = super.getHeaderParams();

            AbstractMap<String, String > params = super.getHeaderParams();
            params.put(TaskConstants.API_PARAMETER.PRIORITY_ID, String.valueOf(entity.PriorityId));
            params.put(TaskConstants.API_PARAMETER.DUEDATE, FormatUrlParameter.formatDate(entity.DueDate));
            params.put(TaskConstants.API_PARAMETER.DESCRIPTION, entity.Description);
            params.put(TaskConstants.API_PARAMETER.COMPLETE, FormatUrlParameter.formatBoolean(entity.Complete));


            FullParameters full = new FullParameters(TaskConstants.OPERATION_METHOD.POST, builder.getUrl(),(HashMap) params, (HashMap) headerParams);

            APIResponse response = this.mExternalRepository.execute(full);

            if (response.statusCode == TaskConstants.STATUS_CODE.SUCCESS){
                result.setResult( new Gson().fromJson(response.json, Boolean.class));
            }else{
                result.setError(super.handleStatusCode(response.statusCode), super.handleErrorMessage(response.json));
            }
        }catch (Exception e) {
            result.setError(super.handleExceptionCode(e), super.handleExceptionMessage(e));
        }

        return result;
    }

    public OperationResult<List<TaskEntity>> getList(int filter) {

        OperationResult<List<TaskEntity>> result = new OperationResult<>();

        try{
            URLBuilder builder = new URLBuilder();

            if (filter == TaskConstants.TASK_FILTER.NO_FILTER){
                builder.addResource(TaskConstants.ENDPOINT.TASK_GET_LIST_NO_FILTER);
            }else if (filter == TaskConstants.TASK_FILTER.OVERDUE){
                builder.addResource(TaskConstants.ENDPOINT.TASK_GET_LIST_OVERDUE);
            }else{
                builder.addResource(TaskConstants.ENDPOINT.TASK_GET_LIST_NEXT_7_DAYS);
            }

            AbstractMap<String, String > headerParams = super.getHeaderParams();


            FullParameters full = new FullParameters(TaskConstants.OPERATION_METHOD.GET, builder.getUrl(),null, (HashMap) headerParams);

            APIResponse response = this.mExternalRepository.execute(full);

            if (response.statusCode == TaskConstants.STATUS_CODE.SUCCESS){
                Type collectionType = new TypeToken<List<TaskEntity>>(){}.getType();
                List<TaskEntity> list = new Gson().fromJson(response.json, collectionType);

                result.setResult(list);

            }else{
                result.setError(super.handleStatusCode(response.statusCode), super.handleErrorMessage(response.json));
            }
        }catch (Exception e) {
            result.setError(super.handleExceptionCode(e), super.handleExceptionMessage(e));
        }

        return result;

    }

    public OperationResult<Boolean> update(TaskEntity taskEntity) {

        OperationResult<Boolean> operationResult = new OperationResult<>();

        try {

            URLBuilder urlBuilder = new URLBuilder();
            urlBuilder.addResource(TaskConstants.ENDPOINT.TASK_UPDATE);

            AbstractMap<String, String> parameters = new HashMap<>();
            parameters.put(TaskConstants.API_PARAMETER.ID, String.valueOf(taskEntity.Id));
            parameters.put(TaskConstants.API_PARAMETER.DESCRIPTION, taskEntity.Description);
            parameters.put(TaskConstants.API_PARAMETER.PRIORITY_ID, String.valueOf(taskEntity.PriorityId));
            parameters.put(TaskConstants.API_PARAMETER.DUEDATE, FormatUrlParameter.formatDate(taskEntity.DueDate));
            parameters.put(TaskConstants.API_PARAMETER.COMPLETE, FormatUrlParameter.formatBoolean(taskEntity.Complete));

            AbstractMap<String, String> headerParameters = super.getHeaderParams();

            FullParameters fullParameters = new FullParameters(TaskConstants.OPERATION_METHOD.PUT, urlBuilder.getUrl(), (HashMap) parameters, (HashMap) headerParameters);

            APIResponse httpResponse = this.mExternalRepository.execute(fullParameters);

            if (httpResponse.statusCode == TaskConstants.STATUS_CODE.SUCCESS) {
                operationResult.setResult(new Gson().fromJson(httpResponse.json, Boolean.class));
            } else {
                operationResult.setError(super.handleStatusCode(httpResponse.statusCode), super.handleErrorMessage(httpResponse.json));
            }
        } catch (Exception e) {
            operationResult.setError(super.handleExceptionCode(e), super.handleExceptionMessage(e));
        }

        return operationResult;
    }

    public OperationResult<Boolean> complete(int id, Boolean complete) {

        OperationResult<Boolean> operationResult = new OperationResult<>();

        try {

            URLBuilder urlBuilder = new URLBuilder();

            if (complete) {
                urlBuilder.addResource(TaskConstants.ENDPOINT.TASK_COMPLETE);
            } else {
                urlBuilder.addResource(TaskConstants.ENDPOINT.TASK_UNCOMPLETE);
            }

            AbstractMap<String, String> parameters = new HashMap<>();
            parameters.put(TaskConstants.API_PARAMETER.ID, String.valueOf(id));

            AbstractMap<String, String> headerParameters = super.getHeaderParams();

            FullParameters fullParameters = new FullParameters(TaskConstants.OPERATION_METHOD.PUT, urlBuilder.getUrl(), (HashMap) parameters, (HashMap) headerParameters);

            APIResponse httpResponse = this.mExternalRepository.execute(fullParameters);

            if (httpResponse.statusCode == TaskConstants.STATUS_CODE.SUCCESS) {
                operationResult.setResult(new Gson().fromJson(httpResponse.json, Boolean.class));
            } else {
                operationResult.setError(super.handleStatusCode(httpResponse.statusCode), super.handleErrorMessage(httpResponse.json));
            }
        } catch (Exception e) {
            operationResult.setError(super.handleExceptionCode(e), super.handleExceptionMessage(e));
        }

        return operationResult;
    }

    public OperationResult<Boolean> delete(int taskId) {

        OperationResult<Boolean> operationResult = new OperationResult<>();

        try {

            URLBuilder urlBuilder = new URLBuilder();
            urlBuilder.addResource(TaskConstants.ENDPOINT.TASK_DELETE);

            AbstractMap<String, String> parameters = new HashMap<>();
            parameters.put(TaskConstants.API_PARAMETER.ID, String.valueOf(taskId));

            AbstractMap<String, String> headerParameters = super.getHeaderParams();

            FullParameters fullParameters = new FullParameters(TaskConstants.OPERATION_METHOD.DELETE, urlBuilder.getUrl(), (HashMap) parameters, (HashMap) headerParameters);

            APIResponse httpResponse = this.mExternalRepository.execute(fullParameters);

            if (httpResponse.statusCode == TaskConstants.STATUS_CODE.SUCCESS) {
                operationResult.setResult(new Gson().fromJson(httpResponse.json, Boolean.class));
            } else {
                operationResult.setError(super.handleStatusCode(httpResponse.statusCode), super. handleErrorMessage(httpResponse.json));
            }
        } catch (Exception e) {
            operationResult.setError(super.handleExceptionCode(e), super.handleExceptionMessage(e));
        }

        return operationResult;
    }

    public OperationResult<TaskEntity> get(int taskId) {

        OperationResult<TaskEntity> operationResult = new OperationResult<>();

        try {

            URLBuilder urlBuilder = new URLBuilder();
            urlBuilder.addResource(TaskConstants.ENDPOINT.TASK_GET_SPECIFIC);
            urlBuilder.addResource(String.valueOf(taskId));

            AbstractMap<String, String> headerParameters = super.getHeaderParams();

            FullParameters fullParameters = new FullParameters(TaskConstants.OPERATION_METHOD.GET, urlBuilder.getUrl(), null, (HashMap) headerParameters);

            APIResponse httpResponse = this.mExternalRepository.execute(fullParameters);

            if (httpResponse.statusCode == TaskConstants.STATUS_CODE.SUCCESS) {

                TaskEntity taskEntity = new Gson().fromJson(httpResponse.json, TaskEntity.class);

                operationResult.setResult(taskEntity);

            } else {
                operationResult.setError(super.handleStatusCode(httpResponse.statusCode), super.handleErrorMessage(httpResponse.json));
            }
        } catch (Exception e) {
            operationResult.setError(super.handleExceptionCode(e), super.handleExceptionMessage(e));
        }

        return operationResult;
    }


}
