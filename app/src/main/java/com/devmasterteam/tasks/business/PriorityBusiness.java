package com.devmasterteam.tasks.business;

import android.content.Context;

import com.devmasterteam.tasks.constants.PriorityCacheConstants;
import com.devmasterteam.tasks.constants.TaskConstants;
import com.devmasterteam.tasks.entities.APIResponse;
import com.devmasterteam.tasks.entities.FullParameters;
import com.devmasterteam.tasks.entities.PriorityEntity;
import com.devmasterteam.tasks.infra.URLBuilder;
import com.devmasterteam.tasks.infra.operations.OperationResult;
import com.devmasterteam.tasks.repository.api.ExternalRepository;
import com.devmasterteam.tasks.repository.local.PriorityRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;

public class PriorityBusiness extends BaseBusiness {

    private ExternalRepository mExternalRepository;
    private PriorityRepository mPriorityRepository;

    public PriorityBusiness(Context context) {
        super(context);
        this.mExternalRepository = new ExternalRepository(context);
        this.mPriorityRepository = PriorityRepository.getInstance(context);
    }

    public OperationResult<Boolean> getList() {
        OperationResult<Boolean> result = new OperationResult<>();

        try{
            URLBuilder builder = new URLBuilder();
            builder.addResource(TaskConstants.ENDPOINT.PRIORITY_GET);

            AbstractMap<String, String > headerParams = super.getHeaderParams();

            FullParameters full = new FullParameters(TaskConstants.OPERATION_METHOD.GET, builder.getUrl(),null, (HashMap) headerParams);

            APIResponse response = this.mExternalRepository.execute(full);

            if (response.statusCode == TaskConstants.STATUS_CODE.SUCCESS){

                Type collectionType = new TypeToken<List<PriorityEntity>>(){}.getType();
                List<PriorityEntity> list = new Gson().fromJson(response.json, collectionType);

                this.mPriorityRepository.clearData();
                this.mPriorityRepository.insert(list);

                //Cria cache de prioridades
                PriorityCacheConstants.setValues(list);


                result.setResult(true);
            }else{
                result.setError(super.handleStatusCode(response.statusCode), super.handleErrorMessage(response.json));
            }
        }catch (Exception e) {
            result.setError(super.handleExceptionCode(e), super.handleExceptionMessage(e));
        }

        return result;
    }

    public List<PriorityEntity> getListLocal(){
        return this.mPriorityRepository.getList();
    }

}
