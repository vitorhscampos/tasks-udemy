package com.devmasterteam.tasks.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.devmasterteam.tasks.business.PersonBusiness;
import com.devmasterteam.tasks.infra.operations.OperationListener;
import com.devmasterteam.tasks.infra.operations.OperationResult;

public class PersonManager {

    private PersonBusiness mPersonBusiness;

    public PersonManager(Context context) {
        this.mPersonBusiness = new PersonBusiness(context);
    }

    public void create(final String name, final String email, final String password, final OperationListener listener) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, OperationResult<Boolean>> task = new AsyncTask<Void, Void, OperationResult<Boolean>>() {
            @Override
            protected OperationResult<Boolean> doInBackground(Void... voids) {
                return mPersonBusiness.create(name, email, password);
            }

            @Override
            protected void onPostExecute(OperationResult<Boolean> result) {
                int error = result.getError();
                if (error != OperationResult.NO_ERROR) {
                    listener.onError(error, result.getErrorMessage());
                } else {
                    listener.onSuccess(result.getResult());
                }

            }
        };

        //Executa a thread
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void login(final String email, final String password, final OperationListener listener) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, OperationResult<Boolean>> task = new AsyncTask<Void, Void, OperationResult<Boolean>>() {
            @Override
            protected OperationResult<Boolean> doInBackground(Void... voids) {
                return mPersonBusiness.login(email, password);
            }

            @Override
            protected void onPostExecute(OperationResult<Boolean> result) {
                int error = result.getError();
                if (error != OperationResult.NO_ERROR) {
                    listener.onError(error, result.getErrorMessage());
                } else {
                    listener.onSuccess(result.getResult());
                }

            }
        };
        //Executa a thread
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
