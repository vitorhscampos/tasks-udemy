package com.devmasterteam.tasks.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.devmasterteam.tasks.business.PriorityBusiness;
import com.devmasterteam.tasks.entities.PriorityEntity;
import com.devmasterteam.tasks.infra.operations.OperationListener;
import com.devmasterteam.tasks.infra.operations.OperationResult;

import java.util.List;

public class PriorityManager {

    private PriorityBusiness mPriorityBusiness;

    public PriorityManager(Context context){
        this.mPriorityBusiness = new PriorityBusiness(context);
    }

    public void getList(final OperationListener listener) {
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, OperationResult<Boolean>> task = new AsyncTask<Void, Void, OperationResult<Boolean>>() {
            @Override
            protected OperationResult<Boolean> doInBackground(Void... voids) {
                return mPriorityBusiness.getList();
            }

            @Override
            protected void onPostExecute(OperationResult<Boolean> result) {
                int error = result.getError();
                if (error != OperationResult.NO_ERROR) {
                    listener.onError(error, result.getErrorMessage());
                } else {
                    listener.onSuccess(result.getResult());
                }

            }
        };

        //Executa a thread
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public List<PriorityEntity> getListLocal(){
        return this.mPriorityBusiness.getListLocal();
    }
}
