package com.devmasterteam.tasks.repository.local;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.devmasterteam.tasks.constants.DataBaseConstants;
import com.devmasterteam.tasks.entities.PriorityEntity;

import java.util.ArrayList;
import java.util.List;

public class PriorityRepository {

    private static PriorityRepository INSTANCE;
    private TaskDataBaseHelper mTaskDataBaseHelper;

    private PriorityRepository(Context context){
        this.mTaskDataBaseHelper = new TaskDataBaseHelper(context);
    }

    public static PriorityRepository getInstance(Context context){

        if (INSTANCE == null){
            INSTANCE = new PriorityRepository(context);
        }
        return INSTANCE;
    }

    public void insert(List<PriorityEntity> list){

        String sql = "insert into " +
                DataBaseConstants.PRIORITY.TABLE_NAME + " (" +
                DataBaseConstants.PRIORITY.COLUMNS.ID + ", " +
                DataBaseConstants.PRIORITY.COLUMNS.DESCRIPTION +
                ") values (?,?)";

        SQLiteDatabase db = this.mTaskDataBaseHelper.getWritableDatabase();

        db.beginTransaction();
        SQLiteStatement statement = db.compileStatement(sql);
        for (PriorityEntity entity : list){
            statement.bindLong(1, entity.Id);
            statement.bindString(2, entity.Description);

            statement.executeInsert();
            statement.clearBindings();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

    }

    public List<PriorityEntity> getList(){
        List<PriorityEntity> list = new ArrayList<>();

        try{

            Cursor cursor;

            SQLiteDatabase db = this.mTaskDataBaseHelper.getReadableDatabase();

            cursor = db.rawQuery("select * from " + DataBaseConstants.PRIORITY.TABLE_NAME, null);

            if (cursor != null && cursor.getCount() > 0){
                while (cursor.moveToNext()){
                    PriorityEntity entity = new PriorityEntity();
                    entity.Id = cursor.getInt(cursor.getColumnIndexOrThrow(DataBaseConstants.PRIORITY.COLUMNS.ID));
                    entity.Description = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseConstants.PRIORITY.COLUMNS.DESCRIPTION));
                    list.add(entity);
                }
            }

            if (cursor != null){
                cursor.close();
            }

            return list;
        }catch (Exception e){
            return list;
        }
    }

    public void clearData() {
        SQLiteDatabase db = this.mTaskDataBaseHelper.getWritableDatabase();
        db.delete(DataBaseConstants.PRIORITY.TABLE_NAME, null, null);
        db.close();

    }
}
