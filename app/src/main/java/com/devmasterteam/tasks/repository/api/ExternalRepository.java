package com.devmasterteam.tasks.repository.api;

import android.content.Context;

import com.devmasterteam.tasks.constants.TaskConstants;
import com.devmasterteam.tasks.entities.APIResponse;
import com.devmasterteam.tasks.entities.FullParameters;
import com.devmasterteam.tasks.infra.InternetNotAvailableException;
import com.devmasterteam.tasks.utils.NetworkUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.channels.NetworkChannel;
import java.util.AbstractMap;
import java.util.Iterator;
import java.util.Map;

public class ExternalRepository {

    private Context mContext;

    public ExternalRepository(Context context){
        this.mContext = context;
    }

    public APIResponse execute(FullParameters parameters) throws InternetNotAvailableException{

        if (!NetworkUtils.isConnectionAvailable(this.mContext))
            throw new InternetNotAvailableException();

        APIResponse response;
        InputStream inputStream;
        HttpURLConnection connection;

        try{
            URL url;

            if (parameters.method == TaskConstants.OPERATION_METHOD.GET){
                url = new URL(parameters.url + getQuery(parameters.parameters, parameters.method));
            }else{
                url = new URL(parameters.url);
            }

            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(1000000);
            connection.setConnectTimeout(1000000);
            connection.setRequestMethod(parameters.method);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", "utf-8");
            connection.setUseCaches(false);

            if (parameters.headerParameters != null){
                Iterator it = parameters.headerParameters.entrySet().iterator();
                while(it.hasNext()){
                    Map.Entry pair = (Map.Entry) it.next();
                    connection.setRequestProperty(pair.getKey().toString(), pair.getValue().toString());
                    it.remove();
                }
            }

            if (parameters.method != TaskConstants.OPERATION_METHOD.GET){
                String query = getQuery(parameters.parameters, parameters.method);

                byte[] postDataBytes = query.getBytes("UTF-8");
                int postDataBytesLength = postDataBytes.length;

                connection.setRequestProperty("Content-Length", Integer.toString(postDataBytesLength));
                connection.setDoOutput(true);
                connection.getOutputStream().write(postDataBytes);

            }

            connection.connect();
            if (connection.getResponseCode() == TaskConstants.STATUS_CODE.SUCCESS) {
                inputStream = connection.getInputStream();
                response = new APIResponse(getStringFromInputStream(inputStream), connection.getResponseCode());
            }else{
                inputStream = connection.getErrorStream();
                response = new APIResponse(getStringFromInputStream(inputStream), connection.getResponseCode());
            }

            inputStream.close();
            connection.disconnect();

        }catch (Exception e){
            response = new APIResponse("", TaskConstants.STATUS_CODE.NOT_FOUND);
        }


        return response;
    }

    private String getStringFromInputStream(InputStream inputStream) {

        if(inputStream == null) {
            return "";
        }

        BufferedReader br;
        StringBuilder builder = new StringBuilder();

        String line;
        try {
           br = new BufferedReader(new InputStreamReader(inputStream));

           while ((line = br.readLine()) != null ){
               builder.append(line);
           }

           br.close();

        }catch (Exception e){
            return "";
        }

        return builder.toString();

    }

    private String getQuery(AbstractMap<String, String> parameters, String method) throws UnsupportedEncodingException{
        if(parameters == null)
            return "";

        StringBuilder builder = new StringBuilder();
        boolean first = true;

        for (Map.Entry<String, String> e: parameters.entrySet()){
            if (first){
                if (method == TaskConstants.OPERATION_METHOD.GET){
                    builder.append("?");
                }
                first = false;
            }else{
                builder.append("&");
            }

            builder.append(URLEncoder.encode(e.getKey(), "UTF-8"));
            builder.append("=");
            builder.append(URLEncoder.encode(e.getValue(), "UTF-8"));
        }

        return builder.toString();
    }
}
