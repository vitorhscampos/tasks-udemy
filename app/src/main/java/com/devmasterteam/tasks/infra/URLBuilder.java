package com.devmasterteam.tasks.infra;

import com.devmasterteam.tasks.constants.TaskConstants;

public class URLBuilder {

    private String url;

    public URLBuilder(){
        this.url = TaskConstants.ENDPOINT.ROOT;
    }

    public void addResource(String value){
        this.url += value + "/";
    }

    public String getUrl(){
        return this.url;
    }
}
