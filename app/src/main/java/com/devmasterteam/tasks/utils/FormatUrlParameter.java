package com.devmasterteam.tasks.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FormatUrlParameter {

    public static String formatBoolean(Boolean value){
        if(value)
            return "true";
        return "false";
    }

    public static String formatDate(Date value){
        if (value == null)
            return "";

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return simpleDateFormat.format(value);

    }
}
